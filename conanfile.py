import os

from conans import ConanFile, tools


class BoolinqConan(ConanFile):
    name = "boolinq"
    version = "3.0.1"
    license = "MIT"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-boolinq/"
    homepage = "https://github.com/k06a/boolinq"
    description = "Simplest C++ header-only LINQ template library"
    topics = ("header-only", "LINQ", "Qt")
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get("https://github.com/k06a/boolinq/archive/{}.zip".format(self.version))
        os.rename("boolinq-{}".format(self.version), "boolinq")
        
    def package(self):
        self.copy("*.h", "include", src="boolinq/include")
